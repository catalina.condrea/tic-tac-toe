var initial = "X";

function clickFunction(myParagraph) {
    var content = document.getElementById(myParagraph);
    
    if(content.innerHTML === "") {
        content.innerHTML = initial;

        initial = initial === "X" ? "0" : "X";

        // if(initial === "X") {
        //     initial = "0";
        // } else {
        //     initial = "X";
        // }
        
        if(checkWinner() && content.innerHTML === "X") {
            document.getElementById("winner").innerHTML = "Winner: ";
            var italic = document.createElement("i");
            var text = document.createTextNode(" X");
            italic.appendChild(text);
            document.getElementById("winner").appendChild(italic);
            return;
        } else if(checkWinner() && content.innerHTML === "0") {
            document.getElementById("winner").innerHTML = "Winner: ";
            var italic = document.createElement("i");
            var text = document.createTextNode(" 0");
            italic.appendChild(text);
            document.getElementById("winner").appendChild(italic);
            return;
        }

        return;
    }
    
    alert("Click another box");
}



// var initial este X pentru ca jocul porneste cu X
// functia executa completarea cu X sau 0 in casuta pe care dai click
// continutul casutei ia valoarea var content
// daca content existent in html este un string gol, atunci da-i valoarea initial (porneste cu X, deci va avea acum valoarea X)
// apoi, daca initial este X atunci da-i valoarea 0 (si astfel initial = 0) / daca initial nu este X atunci da-i valoarea X (si astfel initial = X)
// return - iese din if
// daca dai click pe o patratica deja completata afiseaza alerta error


function checkWinner() {
    var winner = false;
    var a1 = document.getElementById("a1").innerHTML;
    var a2 = document.getElementById("a2").innerHTML;
    var a3 = document.getElementById("a3").innerHTML;
    var b1 = document.getElementById("b1").innerHTML;
    var b2 = document.getElementById("b2").innerHTML;
    var b3 = document.getElementById("b3").innerHTML;
    var c1 = document.getElementById("c1").innerHTML;
    var c2 = document.getElementById("c2").innerHTML;
    var c3 = document.getElementById("c3").innerHTML;

    if(a1 === a2 && a2 === a3 && a1 !== "") {
        document.getElementById("a1Div").style.backgroundColor = "lightblue";
        document.getElementById("a2Div").style.backgroundColor = "lightblue";
        document.getElementById("a3Div").style.backgroundColor = "lightblue";
        return true;
    }

    if(b1 === b2 && b2 === b3 && b1 !== "") {
        document.getElementById("b1Div").style.backgroundColor = "lightblue";
        document.getElementById("b2Div").style.backgroundColor = "lightblue";
        document.getElementById("b3Div").style.backgroundColor = "lightblue";
        return true;
    }

    if(c1 === c2 && c2 === c3 && c1 !== "") {
        document.getElementById("c1Div").style.backgroundColor = "lightblue";
        document.getElementById("c2Div").style.backgroundColor = "lightblue";
        document.getElementById("c3Div").style.backgroundColor = "lightblue";
        return true;
    }

    if(a1 === b1 && b1 === c1 && a1 !== "") {
        document.getElementById("a1Div").style.backgroundColor = "lightblue";
        document.getElementById("b1Div").style.backgroundColor = "lightblue";
        document.getElementById("c1Div").style.backgroundColor = "lightblue";
        return true;
    }

    if(a2 === b2 && b2 === c2 && a2 !== "") {
        document.getElementById("a2Div").style.backgroundColor = "lightblue";
        document.getElementById("b2Div").style.backgroundColor = "lightblue";
        document.getElementById("c2Div").style.backgroundColor = "lightblue";
        return true;
    }

    if(a3 === b3 && b3 === c3 && a3 !== "") {
        document.getElementById("a3Div").style.backgroundColor = "lightblue";
        document.getElementById("b3Div").style.backgroundColor = "lightblue";
        document.getElementById("c3Div").style.backgroundColor = "lightblue";
        return true;
    }

    if(a1 === b2 && b2 === c3 && a1 !== "") {
        document.getElementById("a1Div").style.backgroundColor = "lightblue";
        document.getElementById("b2Div").style.backgroundColor = "lightblue";
        document.getElementById("c3Div").style.backgroundColor = "lightblue";
        return true;
    }

    if(a3 === b2 && b2 === c1 && a3 !== "") {
        document.getElementById("a3Div").style.backgroundColor = "lightblue";
        document.getElementById("b2Div").style.backgroundColor = "lightblue";
        document.getElementById("c1Div").style.backgroundColor = "lightblue";
        return true;
    }

    //if(a1 !== a2 && a2 !== a3 && a1 !== "" && b1 !== b2 && b2 !== b3 && b1 !== "" && c1 !== c2 && c2 !== c3 && c1 !== "") {
    if(a1 !== "" && a2 !== "" && a3 !== "" && b1 !== "" && b2 !== "" && b3 !== "" && c1 !== "" && c2 !== "" && c3 !== "") {
        document.getElementById("winner").innerHTML = "Nobody won ";
        var italic = document.createElement("i");
        var text = document.createTextNode(" :(");
        italic.appendChild(text);
        document.getElementById("winner").appendChild(italic).style.color = "red";
        return false;
    }

    return false;

}
